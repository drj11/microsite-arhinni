[brutal]: #title "Arhinot"
[brutal]: #dateNoDisplay "2023-01-04"
[brutal]: #authorNoDisplay "drj"

<style>
body {
  font-family: 'Andika', sans-serif;
  margin: 0.75rem;
}

h1 {
  font-family: 'Andika', sans-serif;
  font-size: 3rem;
  font-weight: normal;
  margin-bottom: 2px;
  margin-block-start: 0;
}

h2 {
  font-size: 2rem;
  font-weight: normal;
  margin-block-end: 0.75rem;
  margin-block-start: 0.75rem;
  margin-left: -0.75rem;
  margin-right: -0.75rem;
  padding: 0.25rem 0.75rem 0.125rem;
  background-color: #222;
  color: #fff
}

button {
  background-color: #e8a;
  font-family: 'Ubuntu Mono';
}

div > p:first-child {
  margin-top: 0;
}

div > p:last-child {
  margin-bottom: 0;
}

.display {
  font-size: 200%;
  padding: 3rem;
}

.caption.caption {
  font-size: 16px;
  margin: 16px;
  position: absolute;
}

.centre {
  zdisplay: flex;
  zalign-items: center;
  zjustify-content: center;
}

.DarkMid {
  background-color: #000;
  color: #e8a;
}

.DarkLightest {
  background-color: #000;
  color: #fff;
}

.LightestDark {
  background-color: #fff;
  color: #000;
}

.LightestMid {
  background-color: #fff;
  color: #e8a;
}

.MidDark {
  background-color: #e8a;
  color: #000;
}

.MidDark .secondary {
  color: #ffff;
}

.MidLightest {
  background-color: #e8a;
  color: #fff;
}

.secondary {
  font-size: 16px;
}

.ss04 {
  font-feature-settings: "ss04";
}

.ss10 {
  font-feature-settings: "ss10";
}

.frac {
  font-feature-settings: "frac";
}

.onum {
  font-feature-settings: "onum";
}

tt, code, kbd, samp {
    font-family: 'Ubuntu Mono';
}

pre, xmp, plaintext, listing {
    font-family: 'Ubuntu Mono';
}

/* reduce the indent */
ul {
  padding-left: 1em;
}

figure {
    margin-left: 1.5em;
    margin-right: 1.5em;
}

@font-face {
  font-family: 'Andika Bold';
  font-style: bold;
  font-weight: 700;
  font-display: swap;
  src: url(static/TTF/Andika-Bold.ttf) format('opentype');
}

@font-face {
  font-family: 'Andika';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/TTF/Andika-Regular.ttf) format('opentype');
}

/*
Andika-BoldItalic.ttf
Andika-Italic.ttf
*/

@font-face {
  font-family: 'Ubuntu Mono';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(static/TTF/UbuntuMono-Regular.ttf) format('opentype');
}

@font-face {
  font-family: 'Frak';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/TTF/frak.ttf) format('opentype');
}

@font-face {
  font-family: 'Arfboxx';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/TTF/arfboxx.ttf) format('opentype');
}

@font-face {
  font-family: 'Aromech';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/OTF/AromechLAB20221016-Regular.otf) format('opentype');
}

@font-face {
  font-family: 'Aromelt';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/OTF/AromeltLAB20221215-Regular.otf) format('opentype');
}

@font-face {
  font-family: 'Adjuvant';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/OTF/AdjuvantLAB20230110-Regular.otf) format('opentype');
}

@font-face {
  font-family: 'Anglino';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/OTF/AnglinoLAB20230101-Regular.otf) format('opentype');
}

@font-face {
  font-family: 'Astrogn';
  font-style: regular;
  font-weight: 500;
  font-display: swap;
  src: url(static/OTF/AstrognLAB20230208-Regular.otf) format('opentype');
}

</style>

This is not [Arhinni](arhinni.html).
These are the things i made during my ILT Latin Type Design
course while i wasn't making Arhinni, my project font.

Made in <span class="ss10">Yorkshire</a> by David Jones;
mostly in the back half of 2022.

<div class="DarkLightest">
<p class="caption">ax1frak</p>
<div class="DarkMid display centre"
  style="font-family: Frak; font-size: 4rem">
<p>
-abcdefghijklm
</br>
nopqrstuvwxyzſ
</p>
</div>
</div>

This is an exercise in rapidly (1 week) making a basic fraktur
with a modular pattern.
I started drawing a few shapes in Inkscape and then i realised
that all the shapes were made with straight lines, or that
they could be.
So that meant i could use `ttf8` to make a TTF file.
`ttf8` is my own tool and currently has the restriction that it
can only make glyphs that have only straight segments.

Modularity and the strong diagonal were evidently working
through my mind because a few days later i made this alphabet
using a single triangle module used in
just two orientations: ◤ and ◢ (and both together to make a rectangle).

(and yes, a Cyrillic Multiocular O for the funs)

<div class="MidLightest">
<p class="caption">Arfboxx</p>
<div class="MidDark display"
  style="font-family: Arfboxx; border: 8px double">
<p style="font-size: 3rem" contenteditable>
PORTEZ CE VIEUX WHISKY AU JUGE BLOND QUI FUME
</br>
ꙮ
</p>
</div>
</div>


## Serifs

Realising that Arhinni had a taste of reverse contrast (large
serifs and prolonged horizontal strokes put the weight at top
and bottom), i decided i needed some manual practice at heavy
serif lettering. So i roughed out a Roman Rustic alphabet with a
3mm broad nib pen and a high cant of 80 to 90°.
Then i digitised it.

<div class="DarkLightest">
<p class="caption">Aromech</p>
<div class="display" style="font-family: Aromech" contenteditable>
IL MIO VOMBATO STITICO CON AMORE PER TE
</br>
ABCDEFGHIJKLMN
</br>
OPÞQRSTUVWXYZ
</div>
</div>

I cleaned this up and later added numbers.

<div class="DarkLightest">
<p class="caption">Aromelt</p>
<p class="DarkMid display" style="font-family: Aromelt" contenteditable>
THE TORNADO IS COMING
</br>
ABCDEFGHIJKLMN
</br>
OPÞQRSTUVWXYZ
</br>
0123456789
</p>
</div>


## Interlude

While sketching i noticed that my pencil had been smoothed to a
point where it was making the "comic lettering" stroke.
So, not related to my work on Arhinni,
i drew an alphabet to capture the stroke weight and contrast.

I finished digitising this one in January 2023.

<p class="DarkLightest display" style="font-family: Adjuvant" contenteditable>
SIAMO COMICI
</br>
ABCDEFGHIJ
</br>
KLMNOPQRS
</br>
TUVWXYZ
</p>

## Hexagon

While completing the basic alphabet for Arhinni, i noticed that
**b d p q** didn’t require much clearing, shaving, or
adjustment.
A basic **o** plus stem was acceptable or almost so.
This straightforward construction seemed to be because
the basic shape of the **o** left a large clear space at the
top-left and bottom-right.
This meant that when joined to a vertical stem there was a good
amount of space already and no need to create much more.

In effect the **o** has a hexagonal shape;
not a regular hexagon, a hexagon that has two right-angled corners at
bottom-left and top-right, and two parallel 45° angled sides between them.

This led to creating an alphabet based on this somewhat abstract
shape.
First as a pencil sketch, then later traced.

<p class="MidLightest display" style="font-family: Anglino">
a hexagon has six sides
</br>
o
</br>
abcdefghij
</br>
klmnopqr
</br>
sßſtuvw
</br>
xyz
</p>

## Astronomy, Astrology, and Alchemy

At Font School they make you write a brief for your font.
I returned to mine to discover/recall that i had whimsically put
Alchemical Signs in the brief.
It turns out that a good chunk of these are shared with the
[Astronomical Symbols](https://en.wikipedia.org/wiki/Astronomical_symbols);
these are no longer used for modern astronomy, but
are still used in some charts and texts and so on.

I did a warm-up exercise,
drawing the planets (×2 for Uranus) in a monoline style.

<div class="DarkMid">
<p class="caption">Astrogn</p>
<p class="DarkLightest display" style="font-family: Astrogn">
☿♀♁♂♃♄♅♆⛢
</p>
</div>

I was editing Aromelt anyway so i also added the planets (and a sun)
to that,
partly to experiment with relative sizes and balance.

<div class="DarkLightest">
<p class="caption">Aromelt</p>
<p class="DarkMid display" style="font-family: Aromelt">
☉☿♀♁♂♃♄
</p>
</div>

That brings us back to [Arhinni](arhinni.html), which got a somewhat fuller
treatment of the alchemical and the astronomical.

# Sketch on!
