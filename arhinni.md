[brutal]: #title "Arhinni"
[brutal]: #dateNoDisplay "2022-12-26"
[brutal]: #authorNoDisplay "drj"

<style>
body {
  font-family: 'Arhinni Mane', serif;
  margin: 0.75rem;
}

h1 {
  font-family: 'Arhinni Mane', serif;
  font-size: 3rem;
  font-weight: normal;
  margin-bottom: 2px;
  margin-block-start: 0;
}

h2 {
  font-size: 2rem;
  font-weight: normal;
  margin-block-end: 0.75rem;
  margin-block-start: 0.75rem;
  margin-left: -0.75rem;
  margin-right: -0.75rem;
  padding: 0.25rem 0.75rem 0.125rem;
  background-color: #222;
  color: #fff
}

button {
  background-color: #e8a;
  font-family: 'Ubuntu Mono';
}

div > p:first-child {
  margin-top: 0;
}

div > p:last-child {
  margin-bottom: 0;
}

.display {
  font-size: 200%;
  padding: 3rem;
}

.caption.caption {
  font-size: 16px;
  margin: 16px;
  position: absolute;
}

.centre {
  zdisplay: flex;
  zalign-items: center;
  zjustify-content: center;
}

.DarkMid {
  background-color: #000;
  color: #e8a;
}

.DarkLightest {
  background-color: #000;
  color: #fff;
}

.LightestDark {
  background-color: #fff;
  color: #000;
}

.LightestMid {
  background-color: #fff;
  color: #e8a;
}

.MidDark {
  background-color: #e8a;
  color: #000;
}

.MidDark .secondary {
  color: #ffff;
}

.MidLightest {
  background-color: #e8a;
  color: #fff;
}

.secondary {
  font-size: 16px;
}

.ss04 {
  font-feature-settings: "ss04";
}

.ss10 {
  font-feature-settings: "ss10";
}

.frac {
  font-feature-settings: "frac";
}

.onum {
  font-feature-settings: "onum";
}

tt, code, kbd, samp {
    font-family: 'Ubuntu Mono';
}

pre, xmp, plaintext, listing {
    font-family: 'Ubuntu Mono';
}

/* reduce the indent */
ul {
  padding-left: 1em;
}

figure {
    margin-left: 1.5em;
    margin-right: 1.5em;
}

@font-face {
  font-family: 'Arhinni Mane';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(static/OTF/ArhinniLAB20240327-Mane.otf) format('opentype');
}

@font-face {
  font-family: 'Arhinni Raven';
  font-style: bold;
  font-weight: 800;
  font-display: swap;
  src: url(static/OTF/ArhinniLAB20221221-Raven.otf) format('opentype');
}

@font-face {
  font-family: 'Ubuntu Mono';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(static/TTF/UbuntuMono-Regular.ttf) format('opentype');
}

</style>

A hippo-graphic font from <a href="https://about.cubictype.com/">CubicType</a>.

Made in <span class="ss10">Yorkshire</a> by David Jones.

<div class="MidDark display centre">
<p>
AÆBCDEƏƷFGHIJ
<br/>
KLMNƝŊOŒPÞQ
<br/>
RSꞋẞTUVWXYZ
</p>
<p class="secondary">Capitals</p>
<p>
aæbcdðeəʒfghij
<br/>
kĸlmnɲŋoœpþq
<br/>
rsꞌßſtuvwxyz
</p>
<p class="secondary">Little letters</p>
<p>
*0123456789!?
<br/>
¶99$@100%†
</p>
<p class="secondary">Numbers &c</p>
</div>

This font runs, races, and gallops.
Arhinni should be used for your favourite legends,
your cherished poems, and your people’s stories.

Arhinni works best over short distances;
is good at flatwork, but can also handle a few hurdles.
Ride out, let your words trot across the page.

<div class="LightestMid">
<p class="caption">Edit me!</p>
<div class="LightestDark display" style="border: 8px double">
<p style="font-size: 48px" contenteditable>
Try Me, I’m Chonky
</p>
<p style="font-size: 16px" contenteditable>
i’m smol, like icelandic pony
</p>
<p style="font-size: 24px" contenteditable>
Just hoofing it with the herd.
</p>
</div>
</div>

## Design notes

Arhinni is an upright robust display font with strong serifs.
Weight is distributed to tops and bottoms giving a taste of
reverse contrast.
A 45° stroke angle and a calligraphic hint with angular connexions
gives a dynamic feel,
with plentiful moments of tension leaving fewer places to rest.

Single-storey «a» and «g» are available as an OpenType Stylistic
Set.

<div id="ag" class="DarkLightest">
<button class="caption"
onclick="document.getElementById('ag').classList.toggle('ss04')">Toggle Single-storey</button>
<div class="display" contenteditable>
zealous zebras &
quaking quaggas &
orange onagers
</div>
</div>

A partial set of 12 capitals decorated with a ring element are
available as a Stylistic Set.

<div class="ss10 MidLightest">
<p class="caption">Edit me!</p>
<p class="MidDark display" contenteditable>
My Friend’s Horse Kicks Butt;
«A Pink Northern Quagga» Says Your Zebra
</p>
</div>

Many whole numbers are available in both lining and lowercase forms,
try some now:

<div class="ss10 MidLightest">
<p class="caption">Edit me!</p>
<div class="display">
<p id="lnum" oninput="document.getElementById('onum').innerText = this.innerText" class="MidDark lnum" contenteditable>
0123456789
</p>
<p id="onum" oninput="document.getElementById('lnum').innerText = this.innerText" class="MidDark onum" contenteditable>
0123456789
</p>
</div>
</div>

Arhinni comes with a complete set of built-in vulgar fractions
and more can be composed via the `frac` feature.

<div class="frac MidLightest">
<p class="caption">Edit me!</p>
<p class="MidDark display" contenteditable>
The halves 1/2, 3/6, 4/8, and the half knots 5/7, 9/10
</p>
</div>

Arhinni has wide Latin coverage with over 750 glyphs and
support for more than 100 languages,
including localisations for
Catalan, Dutch, Marshallese, Moldovan, Northern Sami, Romanian,
and Turkish.


## Origins

Arhinni began as my project for the I Love Typography
Introduction to Latin Type Design course for Fall 2022.
It recalls an idea from beforetimes of modelling terminals on
the shape of a horse’s hoof.
Arhinni is inspired by Rhiannon, the god of horses in Welsh
myth, and by Epona, the horse in the videogame series The Legend
of Zelda and also a horse god in Gallo–Roman myth.
Rhiannon also gives, by corruption, Arhinni its name.

Typographically the design refers to Albertus, Swift, and Icone
from the 20th century and a little more closely to
Nina Stössinger’s [Nordvest](https://monokrom.no/fonts/nordvest) and
Sandrine Nugue’s [Infini](https://www.cnap.fr/sites/infini/),
both from the 21st century.


## Examples

<p contenteditable>
Only Tofu Eating Wokerati Can Edit This Text.
</p>

We got planets
<p class="DarkLightest display">
☉ Sun •
☿ Mercury •
♀ Venus •
♁ Earth •
♂ Mars •
♃ Jupiter •
♄ Saturn •
♆ Neptune •
⛢ Uranus •
</p>

And a selection of Alchemical supplies
<p class="DarkMid display">
🝪 Alembic •
🜫 Antimony Ore •
🜇 Aqua Regia •
🜈 Aqua Vitæ •
🝫 Bath of Mary •
🜾 Bismuth Ore •
🜠 Copper Ore •
🜡 Iron Copper Ore •
🜜 Iron Ore •
🜪 Lead Ore •
🜱 Regulus of Antimony •
🜘 Rock Salt •
🝜 Stratum Super Stratum •
🝝 Stratum Super Stratum 2 •
🜍 Sulfur •
🜩 Tin Ore •
🜨 Verdigris •
</p>

Some Alchemicals are accessible as ligatures enabled with
Stylistic Set 07.

<p class="MidLightest display">
Ainderby Quernhow † Batley † Coxwold † Doncaster † Emley †
Fylingdales † Grimethorpe † Hebden Bridge † Ilkley †
Just another day in God’s Own Country †
Kippax † Langsett † Mytholmroyd † Netherthong † Ossett † Pontefract †
Queensbury † Rievaulx † SHEFFIELD † Thrybergh † Upperthong †
Vale of York † Wensleydale † YORKSHIRE
</p>

Handy phrases
<div class="MidLightest display">
íoc ⁊ taispeáin
<!--Irish: Pay and Display-->

Arhinni—om katten själv får välja
<!--Swedish: if the cat itself gets to choose-->

Kar seješ to žanješ
<!--Slovenian: reap what you sow-->

Ebule laa azụ, ọ bịa ọgụ
<!-- Igbo: the ram retreasts before attacking an enemy -->
</div>

<div class="LightestMid display onum" style="border: 8px double">
<p>Dragon Valley Pastures for Retired Horses.</p>
<p>Incident free since 2020.</p>
<p>Gochel y ddraig.</p>
</div>


<div class="MidLightest">
<p class="caption">Used by some fantastic brands</p>
<p class="MidDark display" contenteditable>
Rhiannon’s Guided Woodland Walks ⁂
The Flying Dragon: Victuals and Boarding ⁂
Epona’s Horse Riding and Training School
</p>
</div>

<div class="MidLightest">
<p class="caption">Look out for these titles in your local bookshop</p>
<div class="MidDark display">
The Alchemy of Type ¶ Stop Stealing Horses & Dragons ¶
Web Typography for Zebras ¶ The Anatomy of Horses ¶ Thinking with Dragons
</div>
</div>

<!--
It’s reet good.

Th’a’ daft a’peth.

Put t’wood in t’ole.

You’d mek a better door than a windah.

Were tha born in a barn?
-->

<div class="DarkLightest">
<p class="caption">Super Heavy, coming soon, in 2024?</p>
<div class="DarkMid display"
 style="font-family: 'Arhinni Raven'; font-size: 4rem">
NORTHERN
<br/>
RHINESTONE
<br/>
THRONES
</div>
</div>

# Tara rŵan!
