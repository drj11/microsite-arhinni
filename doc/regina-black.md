# Regina Black

2022-12-17

## A critique of colour

http://regina-black.losttype.com/

The Regina Black microsite has much good about it, and this
document is a brief analysis of how it uses colour.

Almost the entire site is done with a 4 colour palette, all featuring
in the Hero image placard:

- Dark Blue ~ #045 (grey #3)
- Dark Pink ~ #E88 (grey #9)
- Light Blue ~ #8AC (grey #a)
- Light Pink ~ #EDC (grey #d)

On the hero: Field is Dark Blue, large element in Light pink, a
secondary element and borders in Light blue, and a tertiary
element in dark pink.

Note that the dark pink is still light enough to provide some
contrast against the dark blue.

(additional colours are: a grey cyan, #688, used in the
Multilingual Support poster for a "greyed out" effect; and a
near white, #EEE, in the numbers poster)

A text block is in light pink, with mainly dark blue text, and
dark pink secondaries.
On this block there is a nice hover over "Purchase Regina Black"
button:
- neutral: dark blue on light pink with dark pink border
- active: dark pink on dark blue with dark pink border.

Tester in dark blue on dark pink with light pink captions.

Alphabet Regular is light pink on dark blue with light blue border and
captions.

Alphabet Regular is dark blue on light pink with dark pink
border and captions.

Book browser has a light blue field, the books themselves using
dark blue and light pink covers.

X&Y card is light pink on dark blue with dark pink ampersand.

Multilingual support is dark blue on light pink with light blue
borders and dark pink secondaries (and an out-of-palette grey
cyan)

Numerous Numerals is dark blue on dark pink with dark blue
border and light pink alternates (and an out-of-palette near
white)

Ornaments & Borders has a dark blue field and both light pink
and dark pink text; light blue border.


## Observations:

Light blue text appears only infrequently: Only as a secondary
on the regular alphabet, against a dark blue field.
Light blue is mostly used for borders;
only once for a (graphic) background.

Plain text (not a display) uses a single colour way: light pink
field with dark blue text and dark pink secondaries.

Principal colour combinations:

Field/Primary/Secondary

- Dark Pink / Dark Blue / Light Pink
  (occasionally swapped)
- Dark Blue / Light Pink / Dark Pink
- Light Pink / Dark Blue / Dark Pink

A simplified sketch:
- Dark Pink background with Dark Blue and Light Pink
- Dark Blue and Link Pink used as either figure/field or
  field/figure and with Dark Pink secondary


## Analysis

4 colours are arranged in lightness as: #3 #9 #A #D

The darkest colour is dark enough to use any of the others for contrast.
The lightest colour is light enough to use either of the two
darkest colours for contrast, but with a strong clarity
preference for the darkest.
The lighter dark colour (#9 Dark Pink) is middle enough to use
either the lightest or the darkest colour for contrast.

# END
