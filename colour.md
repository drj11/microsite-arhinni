# Colour

What about pink?

This post from moonovermarine
https://typo.social/@moonovermarine@piaille.fr/109574921441384137
has a nice pink yarn:

- #FAC pale light
- #E69 fuller
- #D47 dark mid
- #913 dark shade

instead of #3 #9 #A #D
we could have something like

- #000
- #D7A or #E8A ?
- #999
- #FFF
